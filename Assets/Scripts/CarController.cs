﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarController : MonoBehaviour
{
    public MyPath path;
    public bool crashed;
    public bool success;
    public bool spawned = false;
    public NavMeshAgent _agent;

    private List<Point> waypoints;
    private int currentWaypointIndex = 0;
    private bool moved = false;
    private Transform _target;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        //_agent.Warp(waypoints[currentWaypointIndex].transform.position);

        crashed = false;
        success = false;
    }

    private void Update()
    {
        if(path.waypoints != null && waypoints == null)
        {
            waypoints = path.waypoints;
        }
    }

    public void Move()
    {
        if (spawned && _agent != null && moved && gameObject.activeInHierarchy)
        {
            if(currentWaypointIndex < waypoints.Count)
            {
                _target = waypoints[currentWaypointIndex].transform;
                MoveTowards(_target);
                RotateTowards(_target);
            }
        }
    }
    private void MoveTowards(Transform target)
    {
        _agent.SetDestination(target.position);
    }
    private void RotateTowards(Transform target)
    {
        Vector3 direction = (_agent.velocity + transform.position) - transform.position;
        Debug.DrawRay(transform.position, direction,Color.red);
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * _agent.angularSpeed);
    }

    public void Stop()
    {
        if (_agent != null && _agent.isOnNavMesh)
        {
            _agent.ResetPath();
            _agent.speed = 0f;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Point") {
            currentWaypointIndex++;
            moved = true;
        }
        if (other.tag == "Finish Line") { gameObject.SetActive(false); success = true; }
        if(other.tag == "Barricade") { crashed = true; }
        if(other.tag == "Car") { crashed = true; }
    }

    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }
    public Quaternion Rotation
    {
        get { return transform.rotation; }
        set { transform.rotation = value; }
    }
}
