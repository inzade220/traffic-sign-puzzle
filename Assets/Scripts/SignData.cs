﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Direction
{
    ALL,
    FORWARD,
    RIGHT,
    LEFT,
    NONE
}

public enum SignCategory
{
    MANDATORY,
    WARNING,
    INFORMATION
}
public class SignData : MonoBehaviour
{
    [SerializeField] SignCategory category;
    [SerializeField] string signName;
    [SerializeField] string signDescription;
    [SerializeField] Direction direction;
    [SerializeField] Sprite sprite;

    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
        
    }

    private void OnEnable()
    {
        if(signName != null)
        {
            this.name = signName;
        }
        if (sprite != null)
        {
            _image.sprite = sprite;
        }
    }


    #region GETTERS
    public SignCategory Category
    {
        get { return category; }
    }
    public string Name
    {
        get { return signName; }
    }
    public string Description
    {
        get { return signDescription; }
    }
    public Direction Direction
    {
        get { return direction; }
    }
    public Sprite Sprite
    {
        get { return sprite; }
    }
    #endregion
}
