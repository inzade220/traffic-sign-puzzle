﻿using System.Collections.Generic;
using UnityEngine;

class StartState : FSMState
{
    public StartState()
    {
        stateID = StateID.PlacingSigns;
    }
    /*
     Possible Transition:
        Start -> Play
     */
    public delegate void StartAction();
    public static event StartAction OnStart;

    public delegate void HologramSpawnAction();
    public static event HologramSpawnAction OnHoloSpawn;

    public delegate bool PlayButtonAction();
    public static event PlayButtonAction OnPressed;

    

    //Reason: When State have to change to another state.
    public override void DoBeforeEntering()
    {
        //Spawn 2 cars at the beginning
        Debug.Log("STARTED");
        Debug.Log(OnStart.Method.Name);
        OnHoloSpawn();
        OnStart();

    }

    public override void Reason(List<CarController> cars, GameObject trafficController)
    {
        Debug.Log(OnStart.Method.Name);

        if (OnPressed())
        {
            trafficController.GetComponent<TrafficController>().SetTransition(Transition.Play);
        }
    }
    public override void Act(List<CarController> cars, GameObject trafficController)
    {
    }
}
