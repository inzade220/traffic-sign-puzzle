﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

class FailState : FSMState
{
    public delegate void ReloadButton();
    public static event ReloadButton OnFail;
    public FailState()
    {
        stateID = StateID.CarsCrashed;
    }

    public override void DoBeforeEntering()
    {
        OnFail();
    }
    public override void Act(List<CarController> cars, GameObject trafficController)
    {
        foreach(var car in cars)
        {
            car.Stop();
        }
    }

    public override void Reason(List<CarController> cars, GameObject trafficController)
    {

    }
    public override void DoBeforeLeaving()
    {
        
    }
}
