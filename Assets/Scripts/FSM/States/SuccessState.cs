﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

class SuccessState : FSMState
{
    public delegate void ReloadButton();
    public static event ReloadButton OnSuccess;
    public SuccessState()
    {
        stateID = StateID.CarsPassed;
    }

    public override void DoBeforeEntering()
    {
        OnSuccess();
    }
    public override void Act(List<CarController> cars, GameObject trafficController)
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public override void Reason(List<CarController> cars, GameObject trafficController)
    {

    }
}
