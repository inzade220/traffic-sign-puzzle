﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class PlayState : FSMState
{
    public delegate void SpawnAction();
    public static event SpawnAction OnSpawned;
    
    public delegate void OnPlayAction(bool active);
    public static event OnPlayAction OnPathCalculated;
    
    public delegate bool OnSuccessAction();
    public static event OnSuccessAction OnStateSuccess;

    private bool active = false;
    private Spawner _spawner;

    public PlayState(Spawner spawner)
    {
        stateID = StateID.CarsMoving;
        _spawner = spawner;
    }
    
    public override void DoBeforeEntering()
    {
        active = true;
        OnPathCalculated(active);
    }
    public override void Reason(List<CarController> cars, GameObject trafficController)
    {
        if(_spawner.GetNumberOfTotalCarsWillSpawn() == 0)
        {
            List<CarController> activeCars = cars.FindAll(car => car.gameObject.activeInHierarchy);
            if(activeCars.TrueForAll(car => car.success == true))
            {
                trafficController.GetComponent<TrafficController>().SetTransition(Transition.Success);
            }
        }
        foreach (var car in cars)
        {
            if (car.crashed)
            {
                trafficController.GetComponent<TrafficController>().SetTransition(Transition.Fail);
            }
        }
    }
    public override void Act(List<CarController> cars, GameObject trafficController)
    {
        OnSpawned();

        MoveCars(cars);
    }
    
    public void MoveCars(List<CarController> cars)
    {
        foreach(var car in cars)
        {
                car.Move();
        }
    }
}
