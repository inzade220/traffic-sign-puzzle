﻿using System.Collections.Generic;
using UnityEngine;

public class MyPath : MonoBehaviour
{
    public Point[] points;
    public List<Point> waypoints = new List<Point>();
    private void Awake()
    {
        points = GetComponentsInChildren<Point>();
    }

    private void Start()
    {
        PlayState.OnPathCalculated += CalculatePath;

    }
    private void OnDestroy()
    {
        PlayState.OnPathCalculated -= CalculatePath;
    }

    public void CalculatePath(bool active)
    {
        if (active && points != null)
        {
            var point = points[0].GetPossiblePoint();
            waypoints.Add(points[0]);
            for (int i = 1; i < points.Length; i++)
            {
                waypoints.Add(point);
                point = point.GetPossiblePoint();
                if (point == null)
                {
                    break;
                }
            }
        }
    }

    public bool IsFirstPointHasCar()
    {
        return points[0].hasCar;
    }

    public Point GetFirstPoint
    {
        get{ return points[0]; }
    }

}
