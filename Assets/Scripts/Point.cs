﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//[System.Serializable]
public class Point : MonoBehaviour
{
    public List<Direction> keys;
    public List<Point> values;
    public SignBoard signBoard;
    public Dictionary<Direction,Point> neighbors = new Dictionary<Direction, Point>();
    public bool hasCar;

    private Point _possiblePoint;

    public Point()
    {
       
    }

    private void Start()
    {
        for(int i = 0; i < keys.Count; i++)
        {
            AddNeighbor(keys[i], values[i]);
        }
    }

    public void AddNeighbor(Direction direction, Point neighbor)
    {
        if(neighbor == null)
        {
            return;
        }
        if (neighbors.ContainsKey(direction))
        {
            return;
        }
        neighbors.Add(direction, neighbor);
    }

    public Point GetPossiblePoint()
    {
        //Point possiblePoint = new Point();

        if(signBoard !=null && signBoard.hasSign)
        {
            foreach(Direction direction in neighbors.Keys)
            {
                if(signBoard.direction == direction)
                {
                    _possiblePoint = neighbors[direction];
                    break;
                }
            }
        }
        else if(neighbors.Count > 0)
        {
            foreach(Point point in neighbors.Values)
            {
                _possiblePoint = point;
            }
        }
        else
        {
            _possiblePoint = null;
        }

        return _possiblePoint;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 1.2f);
        Gizmos.color = Color.blue;
        foreach(Point point in neighbors.Values)
        {
            Gizmos.DrawLine(transform.position, point.transform.position);
        }
    }
}
