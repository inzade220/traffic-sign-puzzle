﻿using Lean.Touch;
using UnityEngine;
using UnityEngine.EventSystems;
public class DragDropHandler : MonoBehaviour
    , IDragHandler, IBeginDragHandler, IEndDragHandler
{
    Camera m_Camera;

    private SignData signData;
    private Vector3 _initialPosition;
    private RectTransform _rectTransform;
    private void Awake()
    {
        m_Camera = FindObjectOfType<Camera>();
        signData = GetComponent<SignData>();
        _rectTransform = GetComponent<RectTransform>();
         _initialPosition = _rectTransform.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _rectTransform.sizeDelta *= 1.5f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.anchoredPosition += eventData.delta;

        Ray ray = m_Camera.ScreenPointToRay(eventData.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            GameObject signBoard = hit.transform.gameObject;
            if (signBoard.tag == "Sign Board")
            {
                SignBoard board = signBoard.GetComponent<SignBoard>();
                if (!board.hasSign)
                {
                    this.gameObject.SetActive(false);
                    board.setSignData(signData);
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _rectTransform.sizeDelta /= 1.5f;
        _rectTransform.position = _initialPosition;
    }
}
