﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignBoard : MonoBehaviour
{
    public SignData signData;
    public string signName;
    public string description;
    public bool hasSign;
    public Direction direction;

    private Renderer _renderer;
    private bool signDataIsSet = false;

    Camera cam;

    private void Start()
    {
        cam = FindObjectOfType<Camera>();
        hasSign = false;
        _renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        transform.LookAt(cam.transform);
        if (hasSign && !signDataIsSet)
        {
            signName = signData.Name;
            description = signData.Description;
            direction = signData.Direction;
            _renderer.material.mainTexture = signData.Sprite.texture;

            signDataIsSet = true;
        }
    }

    public void setSignData(SignData data)
    {
        if(data != null)
        {
            signData = data;
            hasSign = true;
            signDataIsSet = false;
        }
    }
}
