﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public CarController[] prefab;
        public int amountOfObject;

        public CarController GetRandomPrefab()
        {
            return prefab[Random.Range(0, prefab.Length)];
        }
    }


    #region Singleton
    public static ObjectPooler Instance;

    private void Awake()
    {
        Instance = this;
        foreach (Pool pool in pools)
        {
            carList = new List<CarController>();
            carQueue = new Queue<CarController>();

            int i;
            for (i = 0; i < pool.amountOfObject; i++)
            {


                CarController car = Instantiate(pool.GetRandomPrefab());
                car.enabled = false;
                car.gameObject.SetActive(false);
                carList.Add(car);
                carQueue.Enqueue(car);
            }
        }
    }
    #endregion

    public List<Pool> pools;
    [HideInInspector]
    public List<CarController> carList;
    public Queue<CarController> carQueue;

    private void Start()
    {

    }
    public CarController SpawnCar(Quaternion rotation, Vector3 position)
    {
        CarController car = carQueue.Dequeue();
        if (car == null) { return null; }

        car.Rotation = rotation;
        car.Position = position;
        car.enabled = true;
        car.spawned = true;

        //Give a unique name to car
        int id = UnityEngine.Random.Range(0, 100);
        car.name = "Car ID " + id;

        car.gameObject.SetActive(true);
        carQueue.Enqueue(car);


        return car;
        /*GameObject car = poolObjects.Dequeue();
        if (car == null) { return null; }
        
        //car.path = path;
        car.SetActive(true);
        poolObjects.Enqueue(car);

        return car;*/
    }
}
