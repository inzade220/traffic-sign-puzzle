﻿public interface IStateMachine
{
    void AddState(FSMState state);

    void DeleteState(StateID ID);

    void PerformTransition(Transition transition);
}