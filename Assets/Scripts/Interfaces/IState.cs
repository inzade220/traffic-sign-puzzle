﻿public interface IState
{
    void OnStateExecution();
    void OnStateEnter();
    void OnStateExit();
    string GetName();
}
