﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignboardsVisibilityHandler : MonoBehaviour
{
    public bool allSignboardsFilled;

    private SignBoard[] _signBoards;

    #region Singleton
    public static SignboardsVisibilityHandler Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion
    private void Start()
    {
        _signBoards = GetComponentsInChildren<SignBoard>();
    }

    private void Update()
    {
        allSignboardsFilled =  Array.TrueForAll(_signBoards, x => x.signData != null);
    }
}
