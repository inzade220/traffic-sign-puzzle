﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineProxy : MonoBehaviour
{
    public static void StartCoroutine(IEnumerator coroutineDlg)
    {
        GameObject tempCoroutineGameObject = new GameObject("Temp");
        Debug.Log("Created obj");
        CoroutineProxy proxy = tempCoroutineGameObject.AddComponent<CoroutineProxy>();
        proxy.StartCo(coroutineDlg);
    }

    private void StartCo(IEnumerator enumerator)
    {
        StartCoroutine(Coroutine(enumerator));
    }

    private IEnumerator Coroutine(IEnumerator dlg)
    {
        yield return dlg;
    }
}
