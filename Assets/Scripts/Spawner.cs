﻿using PathCreation;
using System;
using System.Collections;
using System.Reflection;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public SpawnPoint[] spawnPoints;
    float spawnCooldown = 3f;

    ObjectPooler objectPooler;
    bool allSpawned;

    #region Singleton
    Spawner()
    {
    }
    private static readonly object padlock = new object();
    private static Spawner instance = null;
    public static Spawner Instance
    {
        get
        {
            if (instance == null)
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Spawner();
                    }
                }
            }
            return instance;
        }
    }
    #endregion
    
    private void OnEnable()
    {
        PlayState.OnSpawned += SpawnWithCooldown;

        foreach(var point in spawnPoints)
        {
            StartState.OnHoloSpawn += point.SpawnHologram;
            PlayState.OnSpawned += point.DespawnHologram;
        }

    }
    private void OnDestroy()
    {
        StartState.OnHoloSpawn -= SpawnWithCooldown;
        PlayState.OnSpawned -= SpawnWithCooldown;

        foreach (var point in spawnPoints)
        {
            PlayState.OnSpawned -= point.DespawnHologram;
            StartState.OnHoloSpawn -= point.SpawnHologram;
        }
        /*StartState.OnStart -= SpawnWithCooldown;
        PlayState.OnSpawned -= SpawnWithCooldown;*/
    }

    private void OnDisable()
    {
        StartState.OnStart -= SpawnWithCooldown;
        PlayState.OnSpawned -= SpawnWithCooldown;

        foreach (var point in spawnPoints)
        {
            PlayState.OnSpawned -= point.DespawnHologram;
            StartState.OnHoloSpawn -= point.SpawnHologram;
        }
    }
    private void Start()
    {
        objectPooler = ObjectPooler.Instance;
    }

    private void Update()
    {
        allSpawned = Array.TrueForAll(spawnPoints, x => x.amountOfCarToSpawn == 0);
    }
    public void SpawnWithCooldown()
    {
        if (allSpawned)
        {
            Debug.LogWarning("All cars spawned");
            return;
        }
        if (spawnCooldown >= 3)
        {
            SpawnCar();
            spawnCooldown = 0f;
        }
        spawnCooldown += Time.deltaTime;
    }



    private void SpawnCar()
    {
        foreach (var spawnPoint in spawnPoints)
        {
            if (spawnPoint != null && spawnPoint.amountOfCarToSpawn > 0 && spawnPoint.IsSpawnPossible())
            {
                spawnPoint.Spawn();
            }
        }
    }

    public int GetNumberOfTotalCarsWillSpawn()
    {
        int totalCars = 0;
        foreach(var sPoint in spawnPoints)
        {
            totalCars += sPoint.amountOfCarToSpawn;
        }
        return totalCars;
    }
}
