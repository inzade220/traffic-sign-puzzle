﻿using System.Collections.Generic;
using UnityEngine;

public class TrafficController : MonoBehaviour
{
    public List<CarController> cars;
    private FSMSystem _stateMachine;

    private Spawner _spawner;
    private void Start()
    {
        _spawner = FindObjectOfType<Spawner>();
        cars = ObjectPooler.Instance.carList;
        CreateFSM();
    }

    public void SetTransition(Transition transition) { _stateMachine.PerformTransition(transition); }

    private void FixedUpdate()
    {
        _stateMachine.CurrentState.Reason(cars,gameObject);
        _stateMachine.CurrentState.Act(cars, gameObject);
    }

    private void CreateFSM()
    {
        //Create all states which will be used.
        
        StartState startState = new StartState();
        startState.AddTransition(Transition.Play, StateID.CarsMoving);      //Start -> Play

        PlayState playState = new PlayState(_spawner);
        playState.AddTransition(Transition.Fail, StateID.CarsCrashed);      //Play -> Fail
        playState.AddTransition(Transition.Success, StateID.CarsPassed);    //Play -> Success

        FailState failState = new FailState();
        failState.AddTransition(Transition.Start, StateID.PlacingSigns);    //Fail -> Start

        SuccessState successState = new SuccessState();
        successState.AddTransition(Transition.Start, StateID.PlacingSigns); //Success -> Start

        _stateMachine = new FSMSystem();

        //Add created states to states list of FSM
        _stateMachine.AddState(startState);
        _stateMachine.AddState(playState);
        _stateMachine.AddState(failState);
        _stateMachine.AddState(successState);

        //Do first action of start state
        _stateMachine.CurrentState.DoBeforeEntering(); 

    }
}
