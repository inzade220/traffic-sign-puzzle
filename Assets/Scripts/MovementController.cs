﻿using DG.Tweening.Plugins.Core.PathCore;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    /*
     * B(t) = (1-t)^3*P0 + 3(1-t)^2*t*P1 + 3(1-t)^2*t*P2 + t^3*P3
    */
    public NavMeshAgent agent;
    //public Transform targetPosition;
    public MyPath path;

    private int index;
    private float speed;
    private Vector3 movePosition;
    private float t;
    private bool coroutineAllowed;
    private bool isGrounded = false;
    private Point[] points;

    float x = 0f;

    private void Start()
    {
        index = 0;
        t = 0f;
        speed = 0.5f;
        coroutineAllowed = true;
        points = path.points;
        transform.position = points[0].transform.position;
    }

    private void Update()
    {
        if(agent != null)
        {
            agent.SetDestination(points[1].transform.position);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
        {
            Debug.Log("Grounded");
            isGrounded = true;
        }
    }
}
