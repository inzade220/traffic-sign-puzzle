﻿using DG.Tweening.Plugins.Core.PathCore;
using PathCreation;
using PathCreation.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnPoint : MonoBehaviour
{
    public MyPath path;
    public PathCreator pathCreator;
    public int amountOfCarToSpawn;
    public GameObject hologramCarPrefab;

    GameObject holoCar;

    private void Start()
    {
        //hologramCarPrefab.SetActive(false);
    }
    public bool IsSpawnPossible()
    {
        if(path == null) { Debug.LogWarning("Path is null"); return false; }

        return !path.IsFirstPointHasCar();
    }

    public void DecreaseAmountOfCar()
    {
        if(amountOfCarToSpawn > 0)
        {
            amountOfCarToSpawn--;
        }
    }

    public void Spawn()
    {
        CarController car = ObjectPooler.Instance.SpawnCar(transform.rotation, path.GetFirstPoint.transform.position);
        car.path = path;
        DecreaseAmountOfCar();
    }

    public void SpawnHologram()
    {
            holoCar = Instantiate(hologramCarPrefab,path.GetFirstPoint.transform.position, path.GetFirstPoint.transform.rotation);
            //holoCar.SetActive(true);

            holoCar.GetComponent<PathFollower>().pathCreator = this.pathCreator;
    }

    public void DespawnHologram()
    {
        if(holoCar != null)
        {
            holoCar.SetActive(false);
        }
    }
    public List<Point> GetWaypointsOfPath()
    {
        if(path.waypoints != null)
        {
            return path.waypoints;
        }
        Debug.LogWarning("Waypoints are null");
        return null;
    }
}
