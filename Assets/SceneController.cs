﻿using Lean.Touch;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public RectTransform signListPanel;
    public RectTransform startStatePanel;
    public RectTransform failStatePanel;
    public RectTransform successStatePanel;
    public Button playBtn;
    public bool playButtonClicked;

    private RectTransform tapToStartPanel;
    

    private void Start()
    {
        tapToStartPanel = startStatePanel.GetComponentInChildren<RectTransform>();
    }

    private void Update()
    {
        if (SignboardsVisibilityHandler.Instance.allSignboardsFilled)
        {
            Debug.Log("A");
            playBtn.gameObject.SetActive(true);
        }
    }
    private void OnEnable()
    {
        StartState.OnStart += OnStartState;
        StartState.OnPressed += PlayButtonClicked;
        FailState.OnFail += OnFailState;
        SuccessState.OnSuccess += OnSuccessState;

    }
    
    private void OnDisable()
    {
        StartState.OnStart -= OnStartState;
        StartState.OnPressed -= PlayButtonClicked;
        FailState.OnFail -= OnFailState;
        SuccessState.OnSuccess -= OnSuccessState;
    }
    public void OnStartState()
    {
        signListPanel.gameObject.SetActive(false);
        startStatePanel.gameObject.SetActive(true);
    }
    public void OnSuccessState()
    {
        successStatePanel.gameObject.SetActive(true);
    }
    public void OnFailState()
    {
        failStatePanel.gameObject.SetActive(true);
    }
    public void TapToStart()
    {
        if (tapToStartPanel.gameObject.activeInHierarchy)
        {
            tapToStartPanel.gameObject.SetActive(false);
            signListPanel.gameObject.SetActive(true);
        }
    }
    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    //Dummy bool check of play button
    //Dummy, but it works
    #region DUMMY CHECK
    public void OnClick()
    {
        playBtn.gameObject.GetComponent<Image>().enabled = false;
        signListPanel.gameObject.SetActive(false);
        playButtonClicked = !playButtonClicked;
        
    }
    public bool PlayButtonClicked()
    {
        return playButtonClicked;
    }
    #endregion
}
